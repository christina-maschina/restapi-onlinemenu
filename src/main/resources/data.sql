INSERT INTO restaurant (name, description, username, image_path)
VALUES
    ('Submarine Burger', 'Officially one of the top burgers in the world, try it and see for yourself', 'burger', 'https://i.ibb.co/dpQy0Zg/submarine.jpg'),
    ('Manga Sushi', 'Best thing Japan ever gave us were Sushi and Manga', 'sushi', 'https://i.ibb.co/f1yN5yL/manga.jpg');

INSERT INTO dish_type (id, name)
VALUES
    (1, 'BURGERI'),
    (2, 'DOMAĆI LIČKI KRUMPIRIĆI'),
    (3, 'SUPERFOOD SALATE'),
    (4, 'MENU ZA KLINCE'),
    (5, 'PIĆA'),
    (6, 'SUSHI BOX'),
    (7, 'SUSHI'),
    (8, 'PREDJELA'),
    (9, 'GLAVNA JELA'),
    (10, 'SALATE'),
    (11, 'UMACI');

INSERT INTO dish (name, image_path, description, price, restaurant_id, dish_type_id)
VALUES
    ('Veliki UzamakiBox', 'https://i.ibb.co/JnqkmwD/uzamaki.jpg', '2 nigiri losos, 6 hosomaki losos, 2 futomoaki losos, 4 California roll, 4 Philadelphia roll, 4 age ebi roll',
     160.00, 2, 6),
    ('Coca Cola', 'https://i.ibb.co/JnqkmwD/uzamaki.jpg', '',
     16.00, 2, 5),
    ('Fanta', 'https://i.ibb.co/JnqkmwD/uzamaki.jpg', '',
     14.00, 1, 5),
    ('Soja', 'https://i.ibb.co/JnqkmwD/uzamaki.jpg', '',
     2.00, 2, 11),
    ('Monster', 'https://i.ibb.co/hMqQNrH/monster.jpg', 'Suoči se sa zvijeri: Četverostruka doza junetine iz lokalnog uzgoja, Special Submarine umak, svježa zelena salata, grilani topljani sir, dimljena dalmatinska panceta i BBQ mayo umak',
     84.00, 1, 1),
    ('Classic', 'https://i.ibb.co/fnG8Brk/classic.jpg', 'Kad su sve što želiš vrući i slani krumpirići, ne traži dalje: naši krumpiriću su svježe rezani i prženi te nikad smrzavani',
     20.00, 1, 2);

