CREATE TABLE IF NOT EXISTS restaurant  (
    id identity,
    name VARCHAR (100) NOT NULL,
    description VARCHAR (255) NOT NULL,
    username VARCHAR (50) NOT NULL,
    image_path VARCHAR(255)
);
CREATE TABLE IF NOT EXISTS  dish_type (
    id identity,
    name VARCHAR (50) NOT NULL
);
CREATE TABLE IF NOT EXISTS dish (
    id identity,
    name VARCHAR (100) NOT NULL,
    image_path VARCHAR (255),
    description VARCHAR (255),
    price DOUBLE NOT NULL,
    restaurant_id BIGINT NOT NULL,
    dish_type_id BIGINT NOT NULL,
    FOREIGN KEY (restaurant_id) REFERENCES restaurant (id),
    FOREIGN KEY (dish_type_id) REFERENCES dish_type (id)
);