package hr.tvz.vidakovic.android.server.demo.service;

import hr.tvz.vidakovic.android.server.demo.model.Dish;
import hr.tvz.vidakovic.android.server.demo.model.DishType;
import hr.tvz.vidakovic.android.server.demo.repository.DishRepository;
import hr.tvz.vidakovic.android.server.demo.repository.DishTypeRepository;
import hr.tvz.vidakovic.android.server.demo.transfer.DishTypeDTO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DishServiceImpl implements DishService {
    private final DishRepository dishRepo;
    private final DishTypeRepository dishTypeRepo;
    private final ModelMapper modelMapper;
    private Set<Long> dishTypeIds = new HashSet<>();

    public DishServiceImpl(DishRepository dishRepo, DishTypeRepository dishTypeRepo, ModelMapper modelMapper) {
        this.dishRepo = dishRepo;
        this.dishTypeRepo = dishTypeRepo;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<DishTypeDTO> getDishTypesByRestaurantId(Long id) {
        List<Dish> dishes = dishRepo.findByRestaurant_id(id);
        dishTypeIds = dishes
                .stream()
                .map(dish -> dish.getDishType().getId())
                .collect(Collectors.toSet());

        List<DishType> dishTypeList = dishTypeRepo.findByIdIn(dishTypeIds);
        Comparator<DishType> compareById = (DishType d1, DishType d2) ->
                d1.getId().compareTo(d2.getId());
        Collections.sort(dishTypeList, compareById);

        return dishTypeList
                .stream()
                .map(dishType -> modelMapper.map(dishType, DishTypeDTO.class))
                .collect(Collectors.toList());

    }
}
