package hr.tvz.vidakovic.android.server.demo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CodeId {
    private Long id;
    private Integer code;
}
