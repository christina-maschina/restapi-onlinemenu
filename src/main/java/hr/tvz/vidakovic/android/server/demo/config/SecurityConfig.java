package hr.tvz.vidakovic.android.server.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.cors();
        http
                .csrf().disable()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("burger")
                .password("$2y$12$hZP.uiFSgEN9fkfqCTrgIOf1HHVF4IQZI15qD2iMJ0yyXg5V4/kwm")
                .roles("RESTAURANT1")
                .and()
                .withUser("sushi")
                .password("$2y$12$M6/w44jSDzRB4G3Npdiqa..Apc57mswbCocEEEeBvd1MTWwPKKfuC")
                .roles("RESTAURANT2")
                .and()
                .withUser("user")
                .password("$2y$12$GJFmHej.wRPyFnpILa4bJeA2/TxGCK71FnAj0CGpSmrq/viY/IbHi")
                .roles("USER");

    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
