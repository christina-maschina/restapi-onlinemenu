package hr.tvz.vidakovic.android.server.demo.service;

import hr.tvz.vidakovic.android.server.demo.command.CodeCommand;
import hr.tvz.vidakovic.android.server.demo.model.CodeId;
import hr.tvz.vidakovic.android.server.demo.model.Restaurant;
import hr.tvz.vidakovic.android.server.demo.repository.RestaurantRepository;
import hr.tvz.vidakovic.android.server.demo.transfer.RestaurantDTO;
import org.modelmapper.ModelMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RestaurantServiceImpl implements RestaurantService {
    private final RestaurantRepository restaurantRepo;
    private final ModelMapper modelMapper;
    private List<CodeId> codeIdList = new ArrayList<>();

    public RestaurantServiceImpl(RestaurantRepository restaurantRepo, ModelMapper modelMapper) {
        this.restaurantRepo = restaurantRepo;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<CodeId> saveCode(CodeCommand codeCommand) {
        CodeId codeId = modelMapper.map(codeCommand, CodeId.class);
        codeIdList.add(codeId);
        return codeIdList;
    }

    @Override
    public Optional<RestaurantDTO> findRestaurantId(String username) {
        Optional<Restaurant> restaurantOptional = restaurantRepo.findByUsername(username);
        if(restaurantOptional.isEmpty()){
            return Optional.ofNullable(null);
        }
        RestaurantDTO restaurantDTO= modelMapper.map(restaurantOptional.get(), RestaurantDTO.class);
        return Optional.of(restaurantDTO);
    }

    @Override
    public Optional<RestaurantDTO> findByCode(Integer code) {
        Optional<CodeId> codeIdOptional = codeIdList.stream()
                .filter(codeId -> codeId.getCode().equals(code))
                .findFirst();
        if (codeIdOptional.isEmpty()) {
            return Optional.ofNullable(null);
        }
        Restaurant restaurant = restaurantRepo.findById(codeIdOptional.get().getId()).orElse(null);
        RestaurantDTO restaurantDTO = modelMapper.map(restaurant, RestaurantDTO.class);

        return Optional.of(restaurantDTO);
    }

    /*@Scheduled(fixedRate = 60000)
    private void clean() {
        codeIdList.clear();
        System.out.println("List cleared");
    }*/
}
