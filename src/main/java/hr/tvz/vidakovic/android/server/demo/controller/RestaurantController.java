package hr.tvz.vidakovic.android.server.demo.controller;

import hr.tvz.vidakovic.android.server.demo.command.CodeCommand;
import hr.tvz.vidakovic.android.server.demo.model.CodeId;
import hr.tvz.vidakovic.android.server.demo.service.RestaurantService;
import hr.tvz.vidakovic.android.server.demo.transfer.RestaurantDTO;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.support.ServletContextResource;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


@RestController
@RequestMapping("/restaurant")
@CrossOrigin("*")
public class RestaurantController {

    private final RestaurantService restaurantService;
    private final ServletContext servletContext;

    public RestaurantController(RestaurantService restaurantService, ServletContext servletContext) {
        this.restaurantService = restaurantService;
        this.servletContext = servletContext;
    }

    @PostMapping
    public ResponseEntity<List<CodeId>> saveCode(@RequestBody final CodeCommand codeCommand) {

        return new ResponseEntity<>(restaurantService.saveCode(codeCommand), HttpStatus.CREATED);
    }

    @GetMapping("/{code}")
    public ResponseEntity<RestaurantDTO> getByCode(@PathVariable final Integer code) {
        return restaurantService.findByCode(code)
                .map(restaurantDTO -> ResponseEntity
                        .status(HttpStatus.OK)
                        .body(restaurantDTO))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}
