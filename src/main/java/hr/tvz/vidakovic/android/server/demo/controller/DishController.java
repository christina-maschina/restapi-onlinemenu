package hr.tvz.vidakovic.android.server.demo.controller;

import hr.tvz.vidakovic.android.server.demo.service.DishService;
import hr.tvz.vidakovic.android.server.demo.transfer.DishTypeDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("dish")
@CrossOrigin("*")
public class DishController {

    private final DishService dishService;

    public DishController(DishService dishService) {
        this.dishService = dishService;
    }

    @GetMapping("/{id}")
    public List<DishTypeDTO> getDishTypes (@PathVariable Long id){
        return dishService.getDishTypesByRestaurantId(id);
    }
}
