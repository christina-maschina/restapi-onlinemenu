package hr.tvz.vidakovic.android.server.demo.service;

import hr.tvz.vidakovic.android.server.demo.command.CodeCommand;
import hr.tvz.vidakovic.android.server.demo.model.CodeId;
import hr.tvz.vidakovic.android.server.demo.transfer.RestaurantDTO;

import java.util.List;
import java.util.Optional;


public interface RestaurantService {
    List<CodeId> saveCode(CodeCommand codeCommand);
    Optional<RestaurantDTO> findRestaurantId(String username);
    Optional<RestaurantDTO> findByCode(Integer code);
}
