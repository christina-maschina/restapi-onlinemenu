package hr.tvz.vidakovic.android.server.demo.repository;

import hr.tvz.vidakovic.android.server.demo.model.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
    Optional<Restaurant> findByUsername(String username);
    Optional<Restaurant> findById(Long id);
}
