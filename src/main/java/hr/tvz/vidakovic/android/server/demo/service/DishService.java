package hr.tvz.vidakovic.android.server.demo.service;

import hr.tvz.vidakovic.android.server.demo.transfer.DishTypeDTO;

import java.util.List;

public interface DishService {
    List<DishTypeDTO> getDishTypesByRestaurantId (Long id);
}
