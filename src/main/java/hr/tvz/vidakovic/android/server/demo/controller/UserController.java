package hr.tvz.vidakovic.android.server.demo.controller;

import hr.tvz.vidakovic.android.server.demo.model.User;
import hr.tvz.vidakovic.android.server.demo.service.RestaurantService;
import hr.tvz.vidakovic.android.server.demo.transfer.RestaurantDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping(value = "/user", produces = "application/json")
@CrossOrigin("*")
@RestController
public class UserController {
    private final RestaurantService restaurantService;

    public UserController(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    @GetMapping
    public User validateLogin() {
        String username = extractPrincipal(SecurityContextHolder.getContext().getAuthentication());
        Optional<RestaurantDTO> restaurantDTO= restaurantService.findRestaurantId(username);

        if(restaurantDTO.isPresent()){
            return new User("Login successful", restaurantDTO.get().getId());
        }
        return new User("Login successful");
    }

    private static String extractPrincipal(Authentication authentication) {
        if (authentication == null) {
            return null;
        } else if (authentication.getPrincipal() instanceof UserDetails) {
            UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
            return springSecurityUser.getUsername();
        } else if (authentication.getPrincipal() instanceof String) {
            return (String) authentication.getPrincipal();
        }
        return null;
    }
}
