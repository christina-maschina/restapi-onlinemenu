package hr.tvz.vidakovic.android.server.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private String status;
    private Long id;

    public User(String status) {
        this.status = status;
    }
}
