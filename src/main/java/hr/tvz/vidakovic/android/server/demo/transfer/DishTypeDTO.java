package hr.tvz.vidakovic.android.server.demo.transfer;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DishTypeDTO {
    private Long id;
    private String name;
}
