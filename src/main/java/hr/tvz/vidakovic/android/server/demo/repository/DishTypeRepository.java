package hr.tvz.vidakovic.android.server.demo.repository;

import hr.tvz.vidakovic.android.server.demo.model.DishType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface DishTypeRepository extends JpaRepository<DishType, Long> {
    List<DishType> findByIdIn(Set<Long> id);
}
