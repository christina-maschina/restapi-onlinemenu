package hr.tvz.vidakovic.android.server.demo.command;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CodeCommand {
    private Long id;
    private Integer code;

}
