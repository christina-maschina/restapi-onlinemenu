package hr.tvz.vidakovic.android.server.demo.repository;

import hr.tvz.vidakovic.android.server.demo.model.Dish;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DishRepository extends JpaRepository<Dish, Long> {
    List<Dish> findByRestaurant_id(Long id);
}
